# MDict dictionary plugin for OmegaT

[![Build Status](https://dev.azure.com/miurahr/CodeBerg/_apis/build/status/CodeBerg-omegat-mdict?branchName=main)](https://dev.azure.com/miurahr/CodeBerg/_build/latest?definitionId=28&branchName=main)

This is an OmegaT plugin to utilize [MDict](https://mdict.org/) dictionary on [OmegaT CAT(Computer-Aided Translation Tool) application](https://omegat.org/).
This plugin supports MDict format version 2.0.

A development status of the plugin is considered as `Alpha`.

## Supported dictionary and format

The plugin provide text content of dictionary. All images and sounds are ignored.
The plugin also ignores text size. but respect color and type face. 

### Known supported dictionary

(TBD)

## Installation

You can get a plugin jar file from zip distribution file from [Release](https://codeberg.org/miurahr/omegat-mdict/releases)

OmegaT plugin should be placed in `$HOME/.omegat/plugin` or `C:\Program Files\OmegaT\plugin`
depending on your operating system.

## License

This project is distributed under the GNU general public license version 3 or later.
 
- SPDX-License-Identifier: GPL-3.0-or-later
- SPDX-URL: https://spdx.org/licenses/GPL-3.0-or-later.html

 Copyright (C) 2021-2022 Hiroshi Miura

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

# Security Policy

## Supported Versions

| Version | Status                |
|---------|-----------------------|
| 0.3.x   | Development version   |
|  < 0.3  | not supported         |

## Reporting a Vulnerability

Please disclose security vulnerabilities privately at miurahr@linux.com
# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.4.0]

* Require Java 11
* Bump mdict4j@0.5.3
* Bump Gradle@7.6
* Gradle configurations
  * add siging
  * add spotless formatter
* Update javadoc

## [0.3.0]
* Support "@@@LINK=other term" link contents feature.
* Add code of conduct, security policy document
* Bump versions
  * mdict4j@0.4.3
  * gralde-omegat plugin@1.5.9
* Migrate forge site to codeberg.org

## [0.2.2]
* Bump gradle@7.3.3
* Bump base OmegaT@5.7.1

## [0.2.1]
* Fix plugin.link URL

## [0.2.0]
* Fix load method name
* Migrate forge site to codeberg.org
* Bump mdict4j@0.4.0
* Bump jsoup@1.15.2
* Bump gradle@7.3.3

## [0.1.4]
* check key case sensitivity for query and when
  insensitive, qeury with lower case.
* Bump versions
  * mdict4j@0.2.4
  * gradle omegat@1.5.7
  * actions gradle-build-actions@v2
  * actions setup-java@2.5.0
 
## [0.1.3]
* Bump mdict4j@0.2.3
* Support inline image
* Use OmegaT logging

## [0.1.2]
* Use SCM versioning

## [0.1.1]
* Fix Actions syntax

## 0.1.0
* First public release

[Unreleased]: https://codeberg.org/miurahr/omegat-mdict/compare/v0.4.0...HEAD
[0.4.0]: https://codeberg.org/miurahr/omegat-mdict/compare/v0.3.0...v0.4.0
[0.3.0]: https://codeberg.org/miurahr/omegat-mdict/compare/v0.2.2...v0.3.0
[0.2.2]: https://codeberg.org/miurahr/omegat-mdict/compare/v0.2.1...v0.2.2
[0.2.1]: https://codeberg.org/miurahr/omegat-mdict/compare/v0.2.0...v0.2.1
[0.2.0]: https://codeberg.org/miurahr/omegat-mdict/compare/v0.1.4...v0.2.0
[0.1.4]: https://codeberg.org/miurahr/omegat-mdict/compare/v0.1.3...v0.1.4
[0.1.3]: https://codeberg.org/miurahr/omegat-mdict/compare/v0.1.2...v0.1.3
[0.1.2]: https://codeberg.org/miurahr/omegat-mdict/compare/v0.1.1...v0.1.2
[0.1.1]: https://codeberg.org/miurahr/omegat-mdict/compare/v0.1.0...v0.1.1

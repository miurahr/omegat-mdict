plugins {
    java
    checkstyle
    distribution
    signing
    id("com.github.spotbugs") version "5.0.9"
    id("com.diffplug.spotless") version "6.7.2"
    id("org.omegat.gradle") version "1.5.11"
    id("com.palantir.git-version") version "0.13.0" apply false
}

omegat {
    version = "6.0.0"
    pluginClass = "tokyo.northside.omegat.mdict.MDict"
    packIntoJarFileFilter = {it.exclude("META-INF/**/*", "module-info.class", "kotlin/**/*")}
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

repositories {
    mavenCentral()
}

dependencies {
    packIntoJar("tokyo.northside:mdict4j:0.5.3")
    packIntoJar("org.jsoup:jsoup:1.15.3")
}

checkstyle {
    isIgnoreFailures = true
    toolVersion = "7.1"
}

distributions {
    main {
        contents {
            from(tasks["jar"], "README.md", "COPYING", "CHANGELOG.md")
        }
    }
}

// we handle cases without .git directory
val home = System.getProperty("user.home")
val javaHome = System.getProperty("java.home")
val dotgit = project.file(".git")

val signKey = listOf("signingKey", "signing.keyId", "signing.gnupg.keyName").find {project.hasProperty(it)}

if (dotgit.exists()) {
    apply(plugin = "com.palantir.git-version")
    val versionDetails: groovy.lang.Closure<com.palantir.gradle.gitversion.VersionDetails> by extra
    val details = versionDetails()
    val baseVersion = details.lastTag.substring(1)
    version = when {
        details.isCleanTag -> baseVersion
        else ->  baseVersion + "-" + details.commitDistance + "-" + details.gitHash + "-SNAPSHOT"
    }

    tasks.withType<Sign> {
        onlyIf { details.isCleanTag && (signKey != null) }
    }

} else {
    println("Read version property from gradle.properties.")
}

signing {
    when (signKey) {
        "signingKey" -> {
            val signingKey: String? by project
            val signingPassword: String? by project
            useInMemoryPgpKeys(signingKey, signingPassword)
        }
        "signing.keyId" -> {/* do nothing */}
        "signing.gnupg.keyName" -> {
            useGpgCmd()
        }
    }
    sign(tasks.distZip.get())
    sign(tasks.jar.get())
}

spotless {
    java {
        target(listOf("src/*/java/**/*.java"))
        removeUnusedImports()
        palantirJavaFormat()
        importOrder("org.omegat", "io.github.eb4j", "java", "javax", "", "\\#")
    }
}
